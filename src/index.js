import './index.scss';

import Tchat from './tchat-class';
import Tchatbot from './tchat-bot';
import TchatHistory from './tchat-history';

const helloIntent = {
  sentences: [
    'salut',
    'bonjour',
    'coucou',
    'hello',
    'slt',
    'wsh',
    'yo'
  ],
  response: () => 'Hello :p !!'
};

const timeIntent = {
  sentences: [
    'time',
    'hour',
    'date'
  ],
  response: () => new Date().toLocaleDateString()
};

const clearIntent = {
  sentences: [
    'clean',
    'nettoyer',
    'cls'
  ],
  response: () => {
    localStorage.setItem('chat-history', JSON.stringify([]));
    return 'Bien propre maintenant :D';
  }
};

const dogApiIntent = {
  sentences: [
    'chien',
    'dog',
    'montre un chien'
  ],
  url: 'https://dog.ceo/api/breeds/image/random',
  response: (response) => response.message
};

const chuckApiIntent = {
  sentences: [
    'citation',
    'chuck norris',
    'chuck',
    'joke',
    'blague'
  ],
  url: 'https://api.chucknorris.io/jokes/random',
  response: (response) => response.value
};

const adviceApiIntent = {
  sentences: [
    'conseil',
    'advice'
  ],
  url: 'https://api.adviceslip.com/advice',
  response: (response) => response.slip.advice
};

const boredApiIntent = {
  sentences: [
    'bored',
    'activité',
    'trouve moi une activité',
    'ennui'
  ],
  url: 'https://www.boredapi.com/api/activity?participants=1',
  response: (response) => response.activity
};

const helpIntent = {
  sentences: [
    'help',
    'aide',
    'aide moi',
    'commandes',
    '!help'
  ],
  response: () => `<b>Bot (Spider Man):</b> salut | chien | time<br>
    <br><b>Bot (Captain America):</b> salut | joke | advice<br>
    <br><b>Bot (Iron Man):</b> salut | bored | clear`
};

const tchat = new Tchat();
const tchatHistory = new TchatHistory();

const spiderMan = new Tchatbot('Spider-man', 'https://avatarfiles.alphacoders.com/149/149041.jpg', [helloIntent, dogApiIntent, timeIntent]);
const captainAmerica = new Tchatbot('Captain America', 'https://avatarfiles.alphacoders.com/324/324636.jpg', [helloIntent, chuckApiIntent, adviceApiIntent]);
const ironMan = new Tchatbot('Iron Man', 'https://avatarfiles.alphacoders.com/290/29088.jpg', [helloIntent, boredApiIntent, clearIntent]);
const you = new Tchatbot('Vous', 'https://avatarfiles.alphacoders.com/290/290100.jpg', [helpIntent]);

tchat.run([you, spiderMan, captainAmerica, ironMan], tchatHistory);
