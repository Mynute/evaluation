const Tchat = class Tchat {
  renderHeader() {
    return `
      <header>
        <nav class="navbar navbar-dark bg-dark fixed-top">
          <div class="container-fluid">
            <span class="navbar-brand mb-0 h1">
            <span class="text-danger">
              <strong>MARVEL</strong>
            </span> / 
            <span class="text-primary">Chatbot</span>
            </span>
          </div>
        </nav>
      </header>
    `;
  }

  renderGroupList(botList) {
    return `
      <ul class="list-group list-group-flush sticky-top" style="margin-top: 50px">
        ${botList.map((bot) => this.renderGroupElement(bot)).join('')}
      </ul>
    `;
  }

  renderGroupElement(bot) {
    return `
      <li class="list-group-item d-flex justify-content-between align-items-center">
        <img width="60" class="rounded-circle border border-white border-2" src="${bot.avatar}" />
          <b>${bot.name}</b>
        <span class="badge bg-primary rounded-pill" style="visibility: hidden">0</span>
      </li>
    `;
  }

  renderMessageHistory(history) {
    return `
      <section class="messages-history mt-5" >
  ${history.map((message) => {
    if (message.name === 'Vous') {
      return this.renderSelfMessage(message);
    }
    return this.renderExternalMessage(message);
  }).join('')} 
      </section>
    `;
  }

  renderExternalMessage(message) {
    return `
      <div class="row mt-2">
        <div class="col-6">
          <div class="card text-bg-light">
            <h5 class="card-header">
              <img width="10%" src="${message.avatar}" class="rounded-circle img-thumbnail" alt="...">
              ${message.name}
            </h5>
            <div class="card-body">
              <h5 class="card-title">${message.time}</h5>
              <p class="card-text">
                ${message.message.substring(0, 4) === 'http' ? `<img class="fluid" src="${message.message}"/>` : message.message}
              </p>
            </div>
          </div>
        </div>
        <div class="col-6"></div>
      </div>
    `;
  }

  renderSelfMessage(message) {
    return `
      <div class="row mt-2">
        <div class="col-6"></div>
        <div class="col-6">
          <div class="card text-bg-light">
            <h5 class="card-header">
              <img width="10%" src="${message.avatar}" class="rounded-circle img-thumbnail" alt="...">
              ${message.name}
            </h5>
            <div class="card-body">
              <h5 class="card-title">${message.time}</h5>
              <p class="card-text">${message.message}</p>
            </div>
          </div>
        </div>
      </div>
    `;
  }

  renderTypeBar() {
    return `
      <section class="typing mt-3">
        <div class="row">
          <div class="col-12">
            <div class="input-group mb-3">
                <input id="messageBar" type="text" class="form-control" placeholder="Message">
                <button id="sendBtn" class="btn btn-primary" type="button">Envoyer</button>
              </div>
          </div>
        </div>
      </section>
    `;
  }

  render(botList, history) {
    return `
      <div id="app">
        ${this.renderHeader()}
        <main class="container-fluid mt-3">
          <div class="row">
            <div class="col-3">
              ${this.renderGroupList(botList)}
            </div>
            <div class="col-9">
              ${this.renderMessageHistory(history)}
              ${this.renderTypeBar()}
            </div>
          </div>
        </main>
      </div>
    `;
  }

  async handleMessage(history, botList, message) {
    history.save(botList[0], message);
    this.run(botList, history);
    for (let i = 0; i < botList.length; i += 1) {
      const bot = botList[i];
      bot.analyseMessage(message, botList, history, this, this.addBotResponse);
    }
  }

  addBotResponse(bot, message, botList, history, instance) {
    history.save(bot, message);
    instance.run(botList, history);
  }

  run(botList, history) {
    document.body.innerHTML = this.render(botList, history.load());
    const btn = document.getElementById('sendBtn');
    const bar = document.getElementById('messageBar');
    btn.onclick = () => this.handleMessage(history, botList, bar.value);
    onkeyup = (event) => {
      if (event.key === 'Enter') {
        this.handleMessage(history, botList, bar.value);
      }
    };

    // Will wait a bit for image rendering in case of dog
    setTimeout(() => window.scrollTo(0, document.body.scrollHeight), 200);
  }
};

export default Tchat;
