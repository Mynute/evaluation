const ChatHistory = class ChatHistory {
  save(sender, message) {
    const history = this.load();
    history.push({
      name: sender.name,
      avatar: sender.avatar,
      message,
      time: new Date().toDateString()
    });
    localStorage.setItem('chat-history', JSON.stringify(history));
    return history;
  }

  load() {
    if (localStorage.getItem('chat-history') == null) {
      localStorage.setItem('chat-history', JSON.stringify([]));
    }
    return JSON.parse(localStorage.getItem('chat-history'));
  }
};

export default ChatHistory;
