const Tchatbot = class Tchatbot {
  name;

  avatar;

  intentList;

  constructor(name, avatar, intentList) {
    this.name = name;
    this.avatar = avatar;
    this.intentList = intentList;
  }

  async analyseMessage(message, botlist, history, instance, callback) {
    for (let i = 0; i < this.intentList.length; i += 1) {
      const intent = this.intentList[i];
      for (let j = 0; j < intent.sentences.length; j += 1) {
        if (message === intent.sentences[j]) {
          // Diff entre api et normale
          if (intent.url) {
            this.apiCall(intent, botlist, history, instance, callback);
          } else {
            callback(this, intent.response(), botlist, history, instance);
          }
        }
      }
    }
  }

  async apiCall(intent, botlist, history, instance, callback) {
    const res = await fetch(intent.url);
    const json = await res.json();
    callback(this, intent.response(json), botlist, history, instance);
  }
};

export default Tchatbot;
